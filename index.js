// declaration
const express = require('express');
const edge = require("edge.js");
const expressEdge = require('express-edge');
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require('path');
const fileUpload = require("express-fileupload");
const expressSession = require("express-session");
const connectMongo = require("connect-mongo");
const connectFlash = require("connect-flash");

// declaration of controllers
const createPostController = require("./controllers/createPost");
const homePageController = require("./controllers/homePage");
const storePostController = require("./controllers/storePost");
const getPostController = require("./controllers/getPost");
const createUserController = require("./controllers/createUser");
const storeUserController = require("./controllers/storeUser");
const loginController = require("./controllers/login");
const loginUserController = require("./controllers/loginUser");
const logoutController = require("./controllers/logout");





const app = express();

// mongoose connectivity
mongoose.connect('mongodb://localhost/node-js-blog', { useNewUrlParser: true });

app.use(connectFlash());

const mongoStore = connectMongo(expressSession);

app.use(
    expressSession({
        secret: 'secret',
        store: new mongoStore({
            mongooseConnection: mongoose.connection
        })
    })
);

app.use(fileUpload());

// add functionality to express using 'use'
app.use(express.static('public'));

app.use(expressEdge);

app.set('views', `${__dirname}/views`);

app.use("*", (req, res, next) => {
    edge.global("auth", req.session.userId);
    next();
});

// declaration of body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const storePost = require('./middleware/storePost');
const auth = require("./middleware/auth");
const redirectIfAuthenticated = require('./middleware/redirectIfAuthenticated');


app.get('/', homePageController);
app.get('/posts/new', auth, createPostController);
app.get('/post/:id', getPostController);
app.get('/auth/register', redirectIfAuthenticated, createUserController);
app.get("/auth/login", redirectIfAuthenticated, loginController);
app.get("/auth/logout", auth, logoutController);
app.post("/users/login", redirectIfAuthenticated, loginUserController);
app.post('/posts/store', auth, storePost, storePostController);
app.post('/users/register', redirectIfAuthenticated, storeUserController);
app.use((req, res) => res.render('not-found'));


app.listen(4000, () => { console.log('App listening on 4000...') })
