const mongoose = require('mongoose')
const Post = require('./database/models/Post')

mongoose.connect('mongodb://localhost/node-js-test-blog', { useNewUrlParser: true });

// Post.create({
//     title: 'My test blog post',
//     description: 'test Blog post description',
//     content: 'test Lorem ipsum content.'
// }, (error, post) => {
//     console.log(error, post)
// })

// Post.find({}, (error, posts) => {
//   console.log(error, posts)
// })

// Post.findById("5b309ceca84e99bbb6601904", (error, post) => {
//   console.log(error, post)
// })

// Post.findByIdAndUpdate("5b309b35bd7950bab178d912", {
//   title: 'My first blog post title lorem ipsum'
// }, (error, post) => {
//   console.log(error, post)
// })

// Post.findByIdAndRemove("5c444eace7c57216b1a10dc3", (error) => {
//   console.log(error)
// })

 